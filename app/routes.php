<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('index');
});

Route::get('/register', function () {
    return View::make('register');
});

Route::get('/login', function () {
    return View::make('login');
});
Route::post('/login', function () {
    return View::make('login');
});
Route::resource('sessions', 'SessionsController');

Route::post('/register', 'sessionsController@crearUsuario');

Route::get('/admin', function () {
    return View::make('admin');
});

Route::post('/categoria', function () {
    return View::make('admin');
});


Route::post('/item', 'ItemsController@crearItem');
Route::post('/categoria', 'ItemsController@crearCategoria');