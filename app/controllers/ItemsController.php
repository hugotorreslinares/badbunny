<?php

class ItemsController extends BaseController
{


    public function showAdmin()
    {


        return View::make('admin');
    }

    public function crearItem()
    {

        $titulo = Input::get('titulo');
        $item = new Item();
        $item->titulo = $titulo;
        $item->descripcion = Input::get('descripcion');
        $item->save();
        return View::make('admin', ['mensaje' => 'Item creado:'.$titulo ]);


    }

    public function crearCategoria()
    {
        $titulo = Input::get('titulo');

        $categoria = new Categoria();
        $categoria->titulo = $titulo;
        $categoria->descripcion = Input::get('descripcion');
        $categoria->save();
        //return Redirect::to('admin')->with('mensajecategoria',  'Categoria creada:'.$titulo);
        $categorias = Categoria::all();
        return View::make('admin', ['mensajecategoria' => 'Categoria creada:'.$titulo,'categorias'=>$categorias]);


    }

}