<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showIndex()
	{

		return View::make('index');
	}

    public function showRegister()
    {
        $url = URL::action('SessionsController@crearUsuario');

        return View::make('register', ['url'=>$url]);
    }


    public function showLogin()
    {

        User::create(
            [
                'username'=>'hugotorres',
                'password'=>Hash::make('mariana')

            ]);


        return View::make('login');
    }
}
