@extends('layouts.interno')
@section('content')

<div class="welcome">
    <h1>BadBunny</h1>

    <form class="form-horizontal" action="" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend>Login</legend>

            <!-- Text input-->
            <div class="control-group">
                <label class="control-label" for="email">Email</label>

                <div class="controls">
                    <input id="email" name="email" type="email" placeholder="usuario@email.com" class="input-xlarge"
                           required="">

                </div>
            </div>

            <!-- Password input-->
            <div class="control-group">
                <label class="control-label" for="password">Password</label>

                <div class="controls">
                    <input id="password" name="password" type="password" placeholder="password" class="input-xlarge"
                           required="">

                </div>
            </div>

            <!-- Button -->
            <div class="control-group">
                <label class="control-label" for="entrar"></label>

                <div class="controls">
                    <button id="entrar" name="entrar" class="btn btn-primary">Entrar</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
@stop

