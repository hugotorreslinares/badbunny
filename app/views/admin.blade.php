@extends('layouts.admin')
@section('item')

<h1>BadBunny</h1>
<div style="float:left;width:49%;border:thin solid grey;margin:2px;">
    <form class="form-horizontal" action="item" method="post">
        <fieldset>
            {{@$mensaje}}
            <!-- Form Name -->
            <legend>Item</legend>

            <!-- Text input-->
            <div class="control-group">
                <label class="control-label" for="titulo">Titulo</label>

                <div class="controls">
                    <input id="titulo" name="titulo" type="text" placeholder="usuario@email.com" class="input-xlarge"
                           required="">

                </div>
            </div>

            <!-- Password input-->
            <div class="control-group">
                <label class="control-label" for="descripcion">Descripcion</label>

                <div class="controls">
                    <textarea id="descripcion" name="descripcion" placeholder="descripcion" class="input-xlarge"
                              required=""></textarea>

                </div>
            </div>

            <!-- Button -->
            <div class="control-group">
                <label class="control-label" for="entrar"></label>

                <div class="controls">
                    <button id="entrar" name="entrar" class="btn btn-primary">guardar</button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
@stop

@section('categoria')



<div style="float:left;width:49%;border:thin solid  lightgrey; margin:2px;">
    <form action="categoria" method="post" class="form form-horizontal">
        <fieldset>
           @if(isset($mensajecategoria))
            <div class="well">{{ $mensajecategoria }}</div>
            @endif
            <!-- Form Name -->
            <legend>Categoria</legend>
            <label for="titulocat">Titulo:</label><br> <input type="text" id="titulocat" name="titulo"><br>
            <label for="descripcioncat">Descripcion:</label><br> <textarea size="120" id="descripcioncat"
                                                                           name="descripcion"></textarea><br>
            <div class="control-group">
                <label class="control-label" for="entrar"></label>

                <div class="controls">
                    <button id="entrar" name="entrar" class="btn btn-primary">guardar</button>
                </div>
            </div>
        </fieldset>
    </form>
</div>

{{$categorias}}
@stop

